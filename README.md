BlastEm: The fast and accurate Genesis emulator
===============================================

[BlastEm](https://www.retrodev.com/blastem) is a highly precise multi-system
emulator that emulates the Sega Genesis/Mega Drive, Master System and Game Gear
developed by Michael Pavone.

Despite this high accuracy, even the most demanding software runs at full speed
on modest hardware.

Requirements:
-------------

[BlastEm](https://www.retrodev.com/blastem) depends on SDL2, GLEW and OpenGL,
all architectures that are supported must be able to run the emulator.

**Debian/Ubuntu/GNU/Linux**

    # apt install gcc libasound2-dev libglew-dev libsdl2-dev libsdl2-mixer-dev zlib1g-dev make

Installation:
-------------

Next you need to compile this release.

**Compilation**

    $ make

**Once completed, run the created binary**

    $ ./blastem

**Removing compilation**

    $ make clean

License:
--------

> **BlastEm** is free software distributed under the terms of the
> GNU General Public License version 3 or higher.
> This gives you the right to redistribute and/or modify the program as long
> as you follow the terms of the license. See the file COPYING for full license
> details.
> Binary releases of **BlastEm** are packaged with **GLEW** and **SDL2** which
> have their own licenses.

- Copyright (c) 2012-2019 BlastEm by **Michael Pavone**
- Copyright (c) 2019 Individual work by Carlos Donizete Froes [a.k.a coringao]
